import React from 'react';

export default function withLoader(WrappedComponent) {
  return class extends React.PureComponent {
    render() {
      if (this.props.isLoading) {
        return <div class="loader"></div>
      } else {
        return <WrappedComponent {...this.props} />
      }
    }
  }
}
